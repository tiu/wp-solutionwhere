<?php 
/*
Plugin Name: WP-Solutionwhere
Plugin URI: http://wordpress.tiu11.org
Description: A simple wordpress plugin that pull in solutionwhere courses
Version: 0.0.1
Author: Jigar Patel
*/

// Required PHP files
//include_once( WP_PLUGIN_DIR . "/nusoap-0.9.5/lib/nusoap.php" );

/*
Shortcode for pulling courses
*/
function solutionwhere_shortcode( $attr, $content = null ) { 
    extract( shortcode_atts( array(
      'month' => '', 
      'category' => '', 
      'alpha' => '',
      'location' => '',
      'instructor' => '',
      ), $attr ) );
 
    ob_start();
    ?>
  
    <?php

    return ob_get_clean();
}
add_shortcode('solutionwhere', 'solutionwhere_shortcode');

?>