# WP Solutionwhere

**Contributors:** jpatel, [ansonhoyt](https://twitter.com/ansonhoyt @ansonhoyt)  
**Requires at least:** 3.3.2  
**Tested up to:** 4.1.1  
**Plugin URI:** https://bitbucket.org/tiu/wp-solutionwhere  

## Description

WordPress Plugin for Solutionwhere integration.

## Installation

### With Composer

Update your project's composer.json

Update composer.json

    "repositories": [
      {
      "type": "git",
      "url": "https://bitbucket.org/tiu/wp-solutionwhere"
      }
    ],
    "require": {
      "tiu11/wp-solutionwhere": "dev-master"
    }

Run `composer update`

### Manually

This section describes how to install the plugin and get it working.

1. Upload `wp-solutionwhere` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

## License

## Changelog
